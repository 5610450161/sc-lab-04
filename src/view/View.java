package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class View extends JFrame {

	//private JLabel showProjectName;
	private JLabel showInputM;
	private JLabel showInputN;
	private JTextArea showResults;
	private JButton showButton;
	private JComboBox Combo;
	private JTextField InputField;
	private JTextField InputField2;
	private String str;
	private String[] calMethods = {"Select output","cal1","cal2","cal3","cal4","cal5"};
	
	
	public View() {
		createFrame();
	}

	public void createFrame() {
		/*
		inputArea = new JPanel();
		inputArea.setBorder(BorderFactory.createLineBorder(Color.black));
		inputArea.setSize(400, 360);
		
		ShowTextArea = new JPanel();
		ShowTextArea.setBorder(BorderFactory.createLineBorder(Color.red));
		ShowTextArea.setSize(400, 360);
		*/
		showInputN = new JLabel("Input N");
		showInputN.setBounds(30,50,100,20);
		
		showInputM = new JLabel("Input M");
		showInputM.setBounds(30,70,100,20);

		showButton = new JButton("Enter");
		showButton.setLayout(null);
		showButton.setBounds(225,150,100,50);
		
		showResults = new JTextArea();
		showResults.setBounds(400,0,400,400);
		
		Combo = new JComboBox(calMethods);
		Combo.setBounds(75,150,100,50); //x,y,width,height
		
		/*Combo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
				}
			});  */

		
		InputField = new JTextField("0");
		InputField.setBounds(90,50,200,20);
		
		InputField2 = new JTextField("0");
		InputField2.setBounds(90,70,200,20);
		
		add(showInputN);
		add(showInputM);
		add(showButton);
		add(showResults);
		add(Combo);
		add(InputField);
		add(InputField2);
		setLayout(null);
	}

	//public void setProjectName(String s) {
	//	showProjectName.setText(s);
	//}

	public void setResult(String str) {
		showResults.setText(str);
	}
	
	public void setListener(ActionListener list){
		showButton.addActionListener(list);
		
	}
	


	public int getSelectItem() {
		// TODO Auto-generated method stub
		return Combo.getSelectedIndex();
	}

	public int getCollum() {
		// TODO Auto-generated method stub
		
		return Integer.parseInt(InputField2.getText());
	}

	public int getrow() {
		// TODO Auto-generated method stub
		return Integer.parseInt(InputField.getText());
	}
}