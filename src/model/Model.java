package model;

public class Model
{
	String ans = "";
		
	public String cal1(int n, int m){
		for(int i = 1; i <= n;i++){
			for(int j = 1; j <= m;j++){
				ans += "*";
			}
			ans += "\n";
		}
		return ans;
	}

	public String cal2(int n, int m){
		for(int i = 1; i <= n;i++){
			for(int j = 1; j <= m;j++){
				ans += "*";
			}
			ans += "\n";
		}
		return ans;
	}
	
	public String cal3(int n, int m){
		for(int i = 1; i <= n;i++){
			for(int j = 1; j <= i;j++){
				ans += "*";
				
			}
			ans += "\n";
		}
		return ans;
	}
	
	public String cal4(int n, int m){
		for(int i = 1; i <= n;i++){
			for(int j = 1; j <= m;j++){
				if (j % 2 == 0){
					ans += "*";
				}
				else {
					ans += "-";
				}
			}
			ans += "\n";
		}
		return ans;
	}
	
	public String cal5(int n, int m){
		for(int i = 1; i <= n;i++){
			for(int j = 1; j <= m;j++){
				if((i+j)%2==0) {
					ans += "*";
				}
				else {
					ans += " ";
				}
			}
			ans += "\n";
		}
		return ans;
	}
	
	public String toString(){
		return "0";
	}
}

