package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import model.Model;
import view.View;

public class Controller {
	ActionListener list;
	View frame;
	Model model = new Model();

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (frame.getSelectItem() == 1){

				frame.setResult(model.cal1(frame.getrow(),frame.getCollum()));
			}
			
			if (frame.getSelectItem() == 2){

				frame.setResult(model.cal2(frame.getrow(),frame.getCollum()));
			}
			
			if (frame.getSelectItem() == 3){

				frame.setResult(model.cal3(frame.getrow(),frame.getCollum()));
			}
			
			if (frame.getSelectItem() == 4){

				frame.setResult(model.cal4(frame.getrow(),frame.getCollum()));
			}
			
			if (frame.getSelectItem() == 5){

				frame.setResult(model.cal5(frame.getrow(),frame.getCollum()));
			}
			
			// TODO Auto-generated method stub
			}
		
		}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Controller();
	}

	public Controller() {
		frame = new View();
		frame.setVisible(true);
		frame.setSize(800, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		list = new ListenerMgr();
		frame.setListener(list);
		//setTestCase();
		
	
	
	}
	

	

}